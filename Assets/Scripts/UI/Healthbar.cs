﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformFighter {
    public class Healthbar : MonoBehaviour
    {
        
        [SerializeField]
        public GameObject fill;
        Vector3 originalPos;
        RectTransform rekt;

        private void Awake()
        {
            originalPos = fill.transform.position;
            rekt = GetComponent<RectTransform>();
        }

        public void UpdateHealthbar(Vector2 newScale) {
            fill.transform.localScale = newScale;
           // Debug.Log("Koitti updatee helabaria");
        }
        
        
    }
}

