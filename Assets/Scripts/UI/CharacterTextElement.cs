﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
namespace PlatformFighter {
    public class CharacterTextElement : MonoBehaviour
    {
        CharacterBase cbase;
        [SerializeField]
        GameObject parent;
        [SerializeField]
        float height;
        TextMeshPro text;
        string knocked = "Knocked back", block = "Blocking", attack = "Attacking", defaultState = "Normal", dead = "Dead", airdodge = "Airdodge";

        private void Awake()
        {
            cbase = parent.GetComponent<CharacterBase>();
            text = GetComponent<TextMeshPro>();
        }

        // Update is called once per frame
        void Update()
        {
            transform.position = new Vector3(parent.transform.position.x, parent.transform.position.y + height, 0);
            if (cbase.Blocking && text.text != block) text.text = block;
            else if (cbase.AirDodge && text.text != airdodge) text.text = airdodge;
            else if (cbase.KnockedBack && text.text != knocked) text.text = knocked;
            else if (cbase.Dead && text.text != dead) text.text = dead;
            else if (!cbase.Blocking && !cbase.AirDodge && !cbase.KnockedBack && !cbase.Dead && text.text != defaultState) text.text = defaultState;
        }
    }
}

