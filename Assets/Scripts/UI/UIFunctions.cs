﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PlatformFighter {
    public class UIFunctions : MonoBehaviour
    {
        Scene curScene;
        private void Awake()
        {
            curScene = SceneManager.GetActiveScene();
        }
        public void ResetScene()
        {
            StopAllCoroutines();
            Gamemanager.ResetTimeScale();
            SceneManager.LoadScene(curScene.name);
        }
    }
}

