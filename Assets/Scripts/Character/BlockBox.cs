﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformFighter {
    public class BlockBox : AbstractHurtbox
    {
        Health shieldHealth;
        Vector3 scale;
       
        protected override void Awake()
        {
            base.Awake();
            shieldHealth = GetComponent<Health>();
            shieldHealth.MinHealth = 25;
            scale = transform.localScale;
           
        }

        public override void ProcessHit(int hitType, float damage, Vector3 origin, Vector3 knockbackDir, float baseStrength) {
            shieldHealth.TakeDamage(damage);
            float scaleMultiplier = shieldHealth.CurrentHealth / shieldHealth.MaxHealth;
            transform.localScale = scale * scaleMultiplier;

            Vector3 knockback = knockbackDir;
            knockback.y = 0;

            //is the attacker on the left or the right side?
            if (origin.x < Parent.transform.position.x)
            {
                Mover.ForceRotation(Quaternion.LookRotation(Vector3.forward, Vector3.up));

            }
            else if (origin.x > Parent.transform.position.x)
            {
                Mover.ForceRotation(Quaternion.LookRotation(-Vector3.forward, Vector3.up));
            }


            if (origin.x > Parent.transform.position.x)
            {
                knockback.x = -knockback.x;
            }
            knockback.Normalize();
            //ApplyKnockback only moves the character, doesn't cause state changes to knocked back states
            Mover.ApplyKnockback(knockback * (baseStrength / 2));
            Base.AttackReceived = true;
            Base.ReceivedAttackType = 3;
        }

    }

}

