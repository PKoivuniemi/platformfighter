﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace PlatformFighter {
    public class HitBox : MonoBehaviour
    {
        [SerializeField]
        float activeFrames = 0;
        [SerializeField, Tooltip("The damage the hit deals")]
        float hitDamage = 5;
        [SerializeField, Tooltip("Magnitude of the knockback vector")]
        float hitStrength = 5;
        [SerializeField]
        float lagFramesOnBlock;
        float frameCounter;

        [SerializeField, Tooltip("Direction in which the receiver is launched")]
        Vector3 knockbackDir;

        [SerializeField, Tooltip("Allowed values: 0 = Weak, 1 = Normal, 2 = Strong")]
        int hitType = 0;
        [SerializeField, Tooltip("The main character transform in the hierarchy (first child of root)")]
        GameObject parent;
        [SerializeField]
        GameObject text;
        CharacterBase cbase;
        Collider coll;
        /// <summary>
        /// The tags of enemies hit by currently active attack. Used to prevent the same attack from hitting the same enemy multiple times.
        /// </summary>
        List<string> hitObjects;
        List<GameObject> hurtboxes;

        string enemyTag = "";


        private void Awake()
        {
            cbase = cbase == null ? parent.GetComponent<CharacterBase>() : cbase;
            coll = coll == null ? GetComponent<Collider>() : coll;
           
        }

        void OnEnable()
        {
            frameCounter = 0;
            hitObjects = new List<string>();
            hurtboxes = new List<GameObject>();

            //Attack text
            GameObject banner = Instantiate(text, transform.position - cbase.transform.right * 1.5f, Quaternion.identity);          
            TextMeshPro newText = banner.GetComponentInChildren<TextMeshPro>();
            newText.text = hitType == 0 ? "Weak hit" : hitType == 1 ? "Strong hit" : "Spiral hit";
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawLine(transform.position, transform.position + knockbackDir.normalized * hitStrength);
            //Gizmos.DrawSphere(transform.position + transform.up * coll.bounds.extents.y, transform.localScale.x / 2);
        }

        private void Update()
        {
            if (frameCounter >= activeFrames) {
                gameObject.SetActive(false);
            }
            frameCounter++;
        }

        void FixedUpdate()
        {
            if (hurtboxes.Count != 0 && frameCounter < activeFrames)
            {
                CheckForHits();
            }
        }
        
        void CheckForHits() {
            AbstractHurtbox receiver = null, blockBox = null;
            enemyTag = "";
            bool hitsClashed = false;
            
            foreach (GameObject box in hurtboxes) {

                //if two hitboxes collide, break the loop. No damage or knockback should be applied (could also add a "priority" check for attacks here)
                if (box.gameObject.layer == 11) {
                    hitsClashed = true;
                    break;
                }
                if (box.gameObject.layer == 13) {
                    blockBox = box.GetComponent<AbstractHurtbox>();
                }
                receiver = receiver == null ? box.GetComponent<AbstractHurtbox>() : receiver;
                if (enemyTag == "") enemyTag = box.gameObject.tag;
            }

            if (hitsClashed == false)
            {

                if (blockBox != null)
                {
                    receiver = blockBox;
                    cbase.ApplyBlockLag = true;
                    cbase.LagFrames = lagFramesOnBlock;
                }

                //Add hit things into a list to prevent multihits from singular hitboxes
                if (hitObjects.Count != 0)
                {
                    int alreadyInList = 0;
                    foreach (string obj in hitObjects)
                    {

                        if (obj == enemyTag)
                        {
                            alreadyInList++;
                        }
                    }
                    if (alreadyInList == 0)
                    {

                        receiver.ProcessHit(hitType, hitDamage, parent.transform.position, knockbackDir, hitStrength);
                        hitObjects.Add(enemyTag);
                    }

                }
                //If the list is empty, skip the list check and just deal damage and add the object to the list
                else
                {
                    receiver.ProcessHit(hitType, hitDamage, parent.transform.position, knockbackDir, hitStrength);
                    hitObjects.Add(enemyTag);
                }

            }
            else
            {
                //Adding lagframes on clang for emphasized effect, could have a more robust solution
                //Debug.Log("clang");
                cbase.ApplyBlockLag = true;
                cbase.LagFrames = lagFramesOnBlock;
            }
        }
        
        private void OnTriggerEnter(Collider other)
        {
            GameObject hbox = other.gameObject;
            hurtboxes.Add(hbox);
        }
        
    }
}

