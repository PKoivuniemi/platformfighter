﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformFighter {
    public class Health : MonoBehaviour
    {
        [SerializeField]
        float minHealth = 0, maxHealth = 100, curHealth = 0;

        [SerializeField]
        string identifier = "Untagged";
        public string Identifier { get { return identifier; } set { identifier = value; } }
        [SerializeField]
        Healthbar myBar;
        public float MinHealth { get { return minHealth; } set { minHealth = value; } }
        public float MaxHealth { get { return maxHealth; } set { maxHealth = value; } }
        public float CurrentHealth { get { return curHealth; } set { curHealth = value; } }
        public bool Dead { get; set; } = false;

        private void Awake()
        {
            CurrentHealth = MaxHealth;
        }

        private void Start()
        {
            GameObject hpBar = GameObject.Find("BattlesceneUI/Healthbar" + identifier);
            if (hpBar != null) {
                myBar = hpBar.GetComponent<Healthbar>();
            }
        }

        public void ResetHealth() {
            CurrentHealth = MaxHealth;
            Dead = false;
        }

        public void TakeDamage(float amount) {
            CurrentHealth = CurrentHealth - amount > MinHealth ? CurrentHealth - amount : MinHealth;

            float bar = CurrentHealth / MaxHealth;
            if (myBar != null) myBar.UpdateHealthbar(new Vector2(bar, myBar.fill.transform.localScale.y));

            if (CurrentHealth == MinHealth) {
                Dead = true;
            } 
        }

    }
}

