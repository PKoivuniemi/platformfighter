﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformFighter {
    public class CharacterMovement : AbstractMover
    {
        CharacterController controller;
        CharacterBase cbase;
        StateParameterController spc;

        [SerializeField]
        float speed = 5, airDodgeSpeed = 10, jumpPower = 10f, shortHopPower = 5f, fastFallSpeed = 5, gravity = 9.81f, gravityMultiplier = 1, groundCheckOffset = 0, groundMoveSmoothing = 0, aerialMoveSmoothing = 0;
        float maxFallSpeed = -15;
        
        [SerializeField]
        int jumpsLeft = 2, maxJumps = 2;
        //Boolean used for applying knockback
        bool knockbackApplied = false;

        //public override bool Grounded { get; protected set; } = false;
        public override int JumpsLeft{ get { return jumpsLeft; } set { jumpsLeft = value; } }
        public override bool KnockbackApplied { get { return knockbackApplied; } }

        private Quaternion characterRotation;
        public override Quaternion CharacterRotation { get { return characterRotation; } protected set { characterRotation = value; } }

        [SerializeField]
        LayerMask ground;

        Vector3 moveDir, smoothedMoveDir, knockbackDir, lastFrameMovement;
        //ref variable for smoothing, not used for anything else
        Vector3 velocity;
        float velocityF, velocityX, velocityY, velocityYAA;


        [SerializeField]
        float slowDownRate = .2f;


        private void Awake()
        {
            controller = GetComponent<CharacterController>();
            cbase = GetComponent<CharacterBase>();
            spc = GetComponent<StateParameterController>();
            knockbackDir = Vector3.zero;
        }

        private void Start()
        {
            characterRotation = transform.rotation;
        }


        //Check if the character is on the ground provided jump hasn't been pressed
        bool IsCharacterGrounded() {

            Vector3 leftOrigin = controller.bounds.center;
            leftOrigin.x -= controller.bounds.extents.x;

            Vector3 centerOrigin = controller.bounds.center;

            Vector3 rightOrigin = controller.bounds.center;
            rightOrigin.x += controller.bounds.extents.x;

            Ray rayLeft = new Ray(leftOrigin, Vector3.up * (DirY - groundCheckOffset));
            Ray rayCenter = new Ray(centerOrigin, Vector3.up * (DirY - groundCheckOffset));
            Ray rayRight = new Ray(rightOrigin, Vector3.up * (DirY - groundCheckOffset));


            if (Physics.Raycast(rayLeft, controller.bounds.extents.y + groundCheckOffset, ground)) return true;
            if (Physics.Raycast(rayCenter, controller.bounds.extents.y + groundCheckOffset, ground)) return true;
            if (Physics.Raycast(rayRight, controller.bounds.extents.y + groundCheckOffset, ground)) return true;

            return false;
            
        }


        public override void Move() {
            Grounded = IsCharacterGrounded();

            //Check  if the character is falling / not moving on the Y - axis
            //DirY = Grounded ? 0 : DirY - gravity * gravityMultiplier * Time.deltaTime;
            if (Grounded)
            {
                jumpsLeft = maxJumps;
            }

            DirX = cbase.MovementRestricted || cbase.Dead ? 0 : spc.DirX;
            //Control is lessened during knockback
            if (cbase.KnockedBack) DirX = DirX / 3;


            //Players go through each other if either of them is moving at high enough speed
            if (!Grounded) Physics.IgnoreLayerCollision(10, 10, true);
            else Physics.IgnoreLayerCollision(10, 10, false);
            

            RaycastHit hit;
            bool overGround = Physics.Raycast(transform.position, -Vector3.up * (1 + Mathf.Abs(groundCheckOffset)), out hit, 2f, ground);

            //Move in the direction of ground tangent if standing on a slope
            if (Grounded)
            {


                if (overGround)
                {

                    Vector3 t = Vector3.Cross(hit.normal, Vector3.forward);
                    if (t.magnitude == 0)
                    {
                        t = Vector3.Cross(hit.normal, Vector3.up);
                    }
                    Vector3 tmp = t.normalized * DirX * speed;
                    if (!cbase.KnockedBack) {
                        moveDir = tmp;
                    }
                    

                }
                else {
                    DirY = 0;
                }
   
            }
            else
            {
                //Limit default falling speed...
                DirY = DirY > maxFallSpeed ? DirY -= gravity * gravityMultiplier * Time.deltaTime : maxFallSpeed;
            }

            //... but allow faster falling through input
            if(DirY < 0 && DirY > maxFallSpeed - fastFallSpeed && spc.DirY < 0) DirY += spc.DirY * fastFallSpeed;

            //Direction of movement
            moveDir = new Vector3(DirX * speed, DirY);

            
            if (cbase.KnockedBack && !knockbackApplied)
            {
             
                //DirY = knockbackDir.y + spc.DirY * fastFallSpeed / 5;
                DirY = knockbackDir.y;
                moveDir.x = knockbackDir.x;
                knockbackApplied = true;

            }
            
            else if(!cbase.KnockedBack && knockbackApplied){
                knockbackApplied = false;   
            }


            float smooth = Grounded ? groundMoveSmoothing : aerialMoveSmoothing;

            smoothedMoveDir.x = Mathf.SmoothDamp(lastFrameMovement.x, moveDir.x, ref velocityX, smooth * transform.localScale.x * Time.deltaTime);
            smoothedMoveDir.y = DirY;

            //Knockback movement is an arc
            if (cbase.KnockedBack)
            {
                if (Mathf.Abs(lastFrameMovement.x) > Mathf.Abs(smoothedMoveDir.x) && DirY > 0)
                {
                    // DirY = Mathf.SmoothDamp(DirY, DirY - slowDownRate, ref velocityYAA, 2 * Time.deltaTime);
                    
                    DirY -= slowDownRate;
                }
            }


            if (cbase.AirDodge)
            {
                AirDodge(new Vector3(spc.DirX, spc.DirY));
            }

            controller.Move(smoothedMoveDir * Time.deltaTime);
            MoveDir = smoothedMoveDir;
            lastFrameMovement = smoothedMoveDir;


            //Rotate the character to match current movement direction if on the ground

            if (Grounded && !cbase.KnockedBack) {
                //Quaternion characterRotation = moveDir.x < 0 ? Quaternion.LookRotation(Vector3.forward, Vector3.up) : moveDir.x > 0 ? Quaternion.LookRotation(-Vector3.forward, Vector3.up) : transform.rotation;
                if (moveDir.x < 0)
                {
                    characterRotation = Quaternion.LookRotation(Vector3.forward, Vector3.up);
                }
                else if (moveDir.x > 0)
                {
                    characterRotation = Quaternion.LookRotation(-Vector3.forward, Vector3.up);
                }


                ForceRotation(characterRotation);
                //transform.rotation = characterRotation;   
            }
            //Force rotation to current value of characterRotation in case of odd rotations
            if (transform.rotation != characterRotation)
            {
                ForceRotation(characterRotation);
            }

        }

        

        public override void Jump() {
            //First jump allows full power, any following jumps are weaker
            if (jumpsLeft > 0) {
                DirY = 0;
                if (Grounded)
                {
                    if (cbase.JumpHeld)
                    {
                        DirY = jumpPower;

                    }
                    else DirY = shortHopPower;
                    //transform.position += Vector3.up * (groundCheckOffset + .01f);
                }

                else
                {
                    if (jumpsLeft == maxJumps) jumpsLeft -= 1;
                    DirY = jumpPower * .75f;
                }
                jumpsLeft--;
            }
        }

        public override void AirDodge(Vector3 direction) {
            
            smoothedMoveDir = direction.normalized * airDodgeSpeed;
            DirY = smoothedMoveDir.y;
        }

        public override void ForceRotation(Quaternion rotation)
        {
            characterRotation = rotation;
            transform.rotation = Quaternion.Slerp(transform.rotation, characterRotation, Time.deltaTime * 30);
            //Debug.Log("Forcerotation called on " + gameObject.name);
        }

        public override void ApplyKnockback(Vector3 direction) {
            cbase.KnockedBack = true;
            //knockbackDir = new Vector3(direction.x + DirX / 2, direction.y + DirY);
            knockbackDir = direction;
            knockbackApplied = false;  
        }
    }
}

