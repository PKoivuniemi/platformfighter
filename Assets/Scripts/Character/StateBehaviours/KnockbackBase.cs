﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformFighter {
    public class KnockbackBase : StateMachineBehaviour
    {

        public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
        {
            animator.ResetTrigger("ExitKnockBack");
            
        }
    }
}

