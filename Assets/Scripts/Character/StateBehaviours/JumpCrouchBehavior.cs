﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformFighter
{

    public class JumpCrouchBehavior : StateMachineBehaviour
    {
        // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            animator.SetBool("Attack", false);
            
        }

    }

}