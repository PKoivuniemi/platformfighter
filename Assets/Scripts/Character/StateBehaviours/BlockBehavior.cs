﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformFighter {


    public class BlockBehavior : StateMachineBehaviour
    {
        CharacterBase cbase;
        AbstractMover mover;
        // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
       override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            cbase = cbase == null ? animator.GetComponent<CharacterBase>() : cbase;
            mover = mover == null ? animator.GetComponent<AbstractMover>() : mover;
            cbase.Blocking = true;
        }

        public override void OnStateUpdate(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
        {
            if (mover.KnockbackApplied && cbase.Blocking) {
                cbase.KnockedBack = false;
                /*
                animator.ResetTrigger("KnockedBack");
                animator.ResetTrigger("GroundStateKnockBack");
                animator.ResetTrigger("WeakHitReceived");
                animator.ResetTrigger("NormalHitReceived");
                animator.ResetTrigger("StrongHitReceived");
                */
            } 
        }
        // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
        override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            cbase.Blocking = false;
        }
    }

}

