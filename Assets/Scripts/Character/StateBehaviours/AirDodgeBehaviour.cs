﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformFighter {
    public class AirDodgeBehaviour : StateMachineBehaviour
    {
        CharacterBase cbase;
        Renderer rend;
        [SerializeField]
        float dodgeFrames = 5;
        float counter = 0;
        // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            cbase = cbase == null ? animator.GetComponent<CharacterBase>() : cbase;
            rend = rend == null ? animator.GetComponent<Renderer>() : rend;
            cbase.AirDodge = true;
            counter = 0;
        }
        public override void OnStateUpdate(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
        {
            if (counter < dodgeFrames)
            {
                rend.enabled = !rend.enabled;
            }
            else if(counter >= dodgeFrames) {
                rend.enabled = true;
                cbase.AirDodge = false;
            }
            counter++;
        }
        public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
        {
            if (!rend.enabled) rend.enabled = true;
            if (cbase.AirDodge) cbase.AirDodge = false;
        }
    }
}

