﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace PlatformFighter
{
    public class FallingBehavior : StateMachineBehaviour
    {
        CharacterBase cbase;
        // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (cbase == null) cbase = animator.GetComponent<CharacterBase>();
            animator.SetBool("Jump", false);

        }

    }
}
