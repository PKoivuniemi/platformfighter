﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformFighter {
    public class AttackBehavior : StateMachineBehaviour
    {
        CharacterBase cbase;
        [SerializeField]
        bool jumpCancelable = false;
        [SerializeField]
        float cooldown = 0;
        [SerializeField, Tooltip("The index of the desired hitbox to be activated in the hitbox array of CharacterBase")]
        short hitboxIndex = 0;

        float timer = 0;
        bool attackQueued = false;

        // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            cbase = cbase == null ? animator.GetComponent<CharacterBase>() : cbase;

            cbase.CurrentHitBox = hitboxIndex;

            animator.SetBool("CurrentlyAttacking", true);
            animator.SetBool("Attack", false);
            animator.SetBool("FollowUpAttack", false);
            timer = 0;
            attackQueued = false;
            
        }

        // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
        override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (animator.speed != 0)
            {

                timer += Time.deltaTime;

                if (animator.GetBool("Attack"))
                {
                    attackQueued = true;
                    //animator.SetBool("Attack", false);
                }

                if (jumpCancelable)
                {
                    animator.SetBool("CurrentlyAttacking", timer < cooldown ? true : false);
                }
                else animator.SetBool("CurrentlyAttacking", true);


                if (attackQueued && timer >= cooldown)
                {
                    animator.SetBool("FollowUpAttack", true);
                }

            }
            
        }

        // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
        override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            timer = 0;
            animator.SetBool("CurrentlyAttacking", false);
        }
    }
}

