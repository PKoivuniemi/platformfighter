﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformFighter {
    public class MovementControl : StateMachineBehaviour
    {
        CharacterBase cbase;
        [SerializeField]
        bool movementRestricted = false, jumpRestricted = false, attackRestricted = false;
        [SerializeField, Tooltip("If checked, the set restrictions will be left on after leaving the state")]
        bool keepRestrictions = false;

        public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
        {
            cbase = cbase == null ? animator.GetComponent<CharacterBase>() : cbase;

            cbase.MovementRestricted = movementRestricted;
            cbase.JumpRestricted = jumpRestricted;
            cbase.AttackRestricted = attackRestricted;
            /*
            if (movementRestricted || attackRestricted) {
                animator.SetBool("GroundTransitionBlocked", true);
            }
            */
            
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
        {
            if (!keepRestrictions) {
                if (movementRestricted) cbase.MovementRestricted = false;
                if (jumpRestricted) cbase.JumpRestricted = false;
                if (attackRestricted) cbase.AttackRestricted = false;
                //animator.SetBool("GroundTransitionBlocked", false);
            }
        }
    }
}

