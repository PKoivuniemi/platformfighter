﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformFighter {
    public class KnockedBackBehavior : StateMachineBehaviour
    {
        CharacterBase cbase;
        float timer = 0;
        [SerializeField]
        float exitIn = 0;
       
        // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
           
            cbase = cbase == null ? animator.GetComponent<CharacterBase>() : cbase;
            timer = 0;
        }

        // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
        override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            timer += Time.deltaTime;
            if (timer >= exitIn && !cbase.Dead)
            {
                cbase.KnockedBack = false;
                animator.SetTrigger("ExitKnockBack");
            }
        }
        
    }
}

