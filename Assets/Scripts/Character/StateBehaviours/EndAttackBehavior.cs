﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformFighter {
    public class EndAttackBehavior : StateMachineBehaviour
    {
        CharacterBase cbase;
        [SerializeField]
        bool allowBuffer = false;
        // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            cbase = cbase == null ? animator.GetComponent<CharacterBase>() : cbase;
        }

        // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
        override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (!allowBuffer) {
                animator.SetBool("Attack", false);
                animator.SetBool("FollowUpAttack", false);
            }         
        }
    }
}

