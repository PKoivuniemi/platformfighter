﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformFighter {
    public class AerialDefaultState : StateMachineBehaviour
    {
        CharacterBase cbase;
        // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            cbase = cbase == null ? animator.GetComponent<CharacterBase>() : cbase;
            animator.SetLayerWeight(1, 0);

            if (cbase.OnGround && !cbase.Dead)
            {
                cbase.KnockedBack = false;
            }

        }

        public override void OnStateUpdate(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
        {
            //if (cbase.KnockedBack) animator.SetTrigger("KnockedBack");
        }

        // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
        override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            animator.SetLayerWeight(1, 1);
            
        }
    }
}

