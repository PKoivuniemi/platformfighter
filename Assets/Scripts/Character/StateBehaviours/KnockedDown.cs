﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace PlatformFighter {
    public class KnockedDown : StateMachineBehaviour
    {
        public override void OnStateUpdate(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
        {
            animator.SetBool("GroundTransitionBlocked", true);
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
        {
            animator.SetBool("GroundTransitionBlocked", false);
        }
    }
}

