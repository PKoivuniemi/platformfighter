﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformFighter {

    public class JumpBehavior : StateMachineBehaviour
    {
        CharacterBase cbase;

        // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (cbase == null) cbase = animator.GetComponent<CharacterBase>();

            //If player is still holding when jump is entered, jump higher. Otherwise a short hop is made
            if (animator.GetBool("Jump")) {
                if (Input.GetButton("Jump" + cbase.PlayerIdentifier))
                {
                    cbase.JumpHeld = true;
                }

                cbase.TriggerJump = true;
                animator.SetBool("Jump", false);
            }
            
        }

    }

}

