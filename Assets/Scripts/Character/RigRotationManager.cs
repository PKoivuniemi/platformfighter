﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformFighter {
    public class RigRotationManager : MonoBehaviour
    {
        //This class is used for rotating the rig in the direction of movement during knockback
        CharacterBase cbase;
        AbstractMover mover;
        Quaternion baseRotation, lookLeft, lookRight;

        private void Awake()
        {
            cbase = cbase == null ? GetComponentInParent<CharacterBase>() : cbase;
            mover = mover == null ? GetComponentInParent<AbstractMover>() : mover;
            lookLeft = Quaternion.LookRotation(-Vector3.forward, Vector3.up);
            lookRight = Quaternion.LookRotation(Vector3.forward, Vector3.up);
        }

        // Update is called once per frame
        void Update()
        {
            if (cbase.KnockedBack && (cbase.ReceivedAttackType == 2))
            {
                float angle = Mathf.Atan2(mover.MoveDir.y, mover.MoveDir.x) * Mathf.Rad2Deg;

                transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

                
            }

            if ((cbase.OnGround || !cbase.KnockedBack) && transform.rotation != mover.CharacterRotation)
            {
                 transform.rotation = mover.CharacterRotation;
            }
        }
    }
}

