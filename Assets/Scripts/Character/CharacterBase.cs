﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace PlatformFighter
{
    public class CharacterBase : DamageReceiver
    {
        [SerializeField]
        private AbstractMover mover;
        private Animator anim;
        private CharacterController coll;
        [SerializeField]
        private string playerIdentifier = "";
        int receivedAttackType = 0;

        //Array of hitbox objects that are activated when an appropriate attack is called
        [SerializeField]
        private GameObject[] hitboxes;
        //Object / collider enabled when blocking
        [SerializeField]
        private GameObject blockBox;

        public string PlayerIdentifier { get { return playerIdentifier; } set { playerIdentifier = value; } }

        
        //A bunch of public boolean parameters altered mainly by Mecanim and hurtbox behavior
        
        public bool JumpHeld { get; set; } = false;
        public bool TriggerJump { get; set; } = false;
        public bool Blocking { get; set; } = false;
        public bool AirDodge { get; set; } = false;
        /// <summary>
        /// Used to restrict player movement in some states, for example while crouching
        /// </summary>
        public bool MovementRestricted { get; set; } = false;
        /// <summary>
        /// Used to restrict jumping in some states, for example while knocked back
        /// </summary>
        public bool JumpRestricted { get; set; } = false;
        /// <summary>
        /// Used to restrict attacking in some states, for example while lying on the ground
        /// </summary>
        public bool AttackRestricted { get; set; } = false;
        /// <summary>
        /// Is the player knocked back
        /// </summary>
        public bool KnockedBack { get; set; } = false;
        public bool OnGround { get; set; }

        public bool ApplyBlockLag { get; set; } = false;
        public float LagFrames { get; set; } = 0;

        /// <summary>
        /// When a strike is received, this is set to true to trigger state changes in StateParameterController
        /// </summary>
        public bool AttackReceived { get; set; } = false;
        /// <summary>
        /// Dictates the knockback state to use after receiving a strike. 0 = weak, 1 = normal, 2 = strong, 3 = blocked
        /// </summary>
        public int ReceivedAttackType { get { return receivedAttackType; } set { receivedAttackType = value; } }
        public bool Dead { get { return Health.Dead; } }
  
        // <------------ ------------->

        public short CurrentHitBox { get; set; } = 0;
        protected override Health Health { get; set; }

        private void Awake()
        {

            if (PlayerIdentifier != null && PlayerIdentifier != "")
            {
                Transform[] children = GetComponentsInChildren<Transform>();
                foreach (Transform t in children) {
               
                    t.tag = PlayerIdentifier;

                }
                gameObject.tag = PlayerIdentifier;
            }


            mover = GetComponent<AbstractMover>();
            anim = GetComponent<Animator>();
            coll = GetComponent<CharacterController>();
            if (Health == null) {
                Health = gameObject.AddComponent<Health>();
            }
            Health.Identifier = PlayerIdentifier;
            Health.CurrentHealth = Health.MaxHealth;
        }


        private void Update()
        {

            if (OnGround != mover.Grounded) OnGround = mover.Grounded;

            if (Blocking)
            {
                blockBox.SetActive(true);
            }
            else if (!Blocking && blockBox.activeSelf) {
                blockBox.SetActive(false);
            }
            mover.Move();

            if (TriggerJump && !JumpRestricted && !Dead)
            {
                if (mover.JumpsLeft > 0) mover.Jump();
                TriggerJump = false;
                JumpHeld = false;
            }

            //Force "normal" knockback animation on death to prevent standing while dead
            if (Dead) {
                ReceivedAttackType = 1;
                StartCoroutine(Gamemanager.DeathEffect());
            }
        }

        public override void TakeDamage(float amount)
        {
            Health.TakeDamage(amount);
        }

        void ActivateHitBox(){
            if (AttackRestricted == false) {
                hitboxes[CurrentHitBox].SetActive(true);
            }
        }

        void DisableHitBox() {
            hitboxes[CurrentHitBox].SetActive(false);
        }
    }
}
