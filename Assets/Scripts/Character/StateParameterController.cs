﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace PlatformFighter
{
    public class StateParameterController : MonoBehaviour
    {
        Animator animator;
        CharacterBase cbase;
        AbstractMover movement;

        //strings used for getting input
        string identifier, jump, attack, block, horizontal, vertical;
        public bool JumpPressed { get; set; } = false;
        public float DirX { get; private set; } = 0;
        public float DirY { get; private set; } = 0;

        float lagFrames = 0, lagFrameCounter = 0, blockDropFrameCounter = 0, blockDropLag = 5;


        // Start is called before the first frame update
        void Awake()
        {
            animator = GetComponent<Animator>();
            cbase = GetComponent<CharacterBase>();
            movement = GetComponent<AbstractMover>();
            identifier = movement.GetComponent<CharacterBase>().PlayerIdentifier;
            jump = "Jump" + identifier;
            attack = "Attack" + identifier;
            block = "Block" + identifier;
            horizontal = "Horizontal" + identifier;
            vertical = "Vertical" + identifier;
        }
        

        // Update is called once per frame
        void LateUpdate()
        {
         
            DirX = Input.GetAxisRaw(horizontal);
            DirY = Input.GetAxisRaw(vertical);
            

            //Slow down the character when jump is pressed on the ground
            if (animator.GetBool("Jump") && animator.GetBool("OnGround")) {
                DirX = Mathf.Lerp(DirX, 0f, 3 * Time.deltaTime);
            }


            animator.SetFloat("MoveInputVertical", DirY);
            animator.SetFloat("MoveInputHorizontal", DirX);
            animator.SetBool("Moving", movement.DirX != 0);
            animator.SetBool("OnGround", movement.Grounded);

            if (Input.GetButtonDown(attack) && cbase.AttackRestricted == false) {
                animator.SetBool("Attack", true);
            }

            //if (Input.GetButtonDown("Jump") && movement.JumpsLeft > 0 && !animator.GetBool("CurrentlyAttacking"))
            if (Input.GetButtonDown(jump) && movement.JumpsLeft > 0)
            {
                animator.SetBool("Jump", true);
            }



            animator.SetBool("Falling", movement.DirY < 0 && movement.Grounded == false && animator.GetBool("Jump") == false);


            //bool blockpressed = Input.GetAxisRaw("Block" + identifier) <= 0 ? false : true;

            //animator.SetBool("Block", Input.GetAxisRaw("Block" + identifier) > 0 && !animator.GetBool("Blocking"));
            if (Input.GetAxisRaw(block) > 0 && !cbase.AttackRestricted)
            {
                animator.SetBool("Blocking", true);
            }
            else {
                blockDropFrameCounter++;
            }
            if (blockDropFrameCounter >= blockDropLag) {
                animator.SetBool("Blocking", false);
                blockDropFrameCounter = 0;
            }
            //animator.SetBool("Blocking", Input.GetAxisRaw("Block" + identifier) > 0);

           

            //Go into knockback upon receiving an attack
            if (cbase.AttackReceived) {
                cbase.AttackReceived = false;
                animator.SetTrigger("KnockedBack");
                animator.SetTrigger("GroundStateKnockBack");
                cbase.KnockedBack = true;
                
                
                int random = Random.Range(0, 2);

                switch (cbase.ReceivedAttackType) {
                    case 0:
                        //weak hit
                        animator.SetInteger("HitAnim", random);

                        animator.ResetTrigger("NormalHitReceived");
                        animator.ResetTrigger("StrongHitReceived");
                        animator.SetTrigger("WeakHitReceived");
                        break;
                    case 1:
                        //normal hit
                        animator.ResetTrigger("StrongHitReceived");
                        animator.ResetTrigger("WeakHitReceived");
                        animator.SetTrigger("NormalHitReceived");
                        break;
                    case 2:
                        //strong hit
                        animator.ResetTrigger("WeakHitReceived");
                        animator.ResetTrigger("NormalHitReceived");
                        animator.SetTrigger("StrongHitReceived");
                        break;
                    case 3:
                        //blocked hit
                        animator.ResetTrigger("KnockedBack");
                        //cbase.KnockedBack = false;
                        break;
                }
                
            }


            if (cbase.ApplyBlockLag)
            {
                animator.speed = 0;
                lagFrames = cbase.LagFrames;
                lagFrameCounter = 0;
                cbase.ApplyBlockLag = false;
            }
            if(!cbase.ApplyBlockLag && lagFrameCounter >= lagFrames) {
                if (animator.speed != 1) animator.speed = 1;
            }

            animator.SetBool("Dead", cbase.Dead);

            lagFrameCounter++;
        }
    }
}

