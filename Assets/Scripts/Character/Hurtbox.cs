﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformFighter {
    public class Hurtbox : AbstractHurtbox
    {
        private void Update()
        {
            if (Base.AirDodge) Box.enabled = false;
            else Box.enabled = true;
        }

        //Take damage and send signals to characterbase for determining the type of knockback,
        //origin is used for making the receiver face the attacker etc.
        public override void ProcessHit(int hitType, float damage, Vector3 origin, Vector3 knockbackDir, float baseStrength)
        {
            Vector3 knockback = knockbackDir;

            //is the attacker on the left or the right side?
            if (origin.x < Parent.transform.position.x)
            {
                Mover.ForceRotation(Quaternion.LookRotation(Vector3.forward, Vector3.up));

            }
            else if (origin.x > Parent.transform.position.x)
            {
                Mover.ForceRotation(Quaternion.LookRotation(-Vector3.forward, Vector3.up));
            }


            if (origin.x > Parent.transform.position.x)
            {
                knockback.x = -knockback.x;
            }
            knockback.Normalize();

            //AttackReceived causes the switch to a knocked back state, the state is determined by ReceivedAttackType
            if (Base.Blocking) Base.Blocking = false;
            Base.AttackReceived = true;
            Base.ReceivedAttackType = hitType;

            //ApplyKnockback only moves the character, doesn't cause state changes to knocked back states
            Mover.ApplyKnockback(knockback * baseStrength);

            Base.TakeDamage(damage);          
        }
    }
}

