﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformFighter {
    public class Gamemanager: MonoBehaviour
    {      
        public static IEnumerator DeathEffect() {
            Time.timeScale = .5f;
            yield return new WaitForSeconds(1);
            Time.timeScale = 1;
        }
        public static void ResetTimeScale() {
            if (Time.timeScale != 1) Time.timeScale = 1;
        }
    }
}

