﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformFighter {
    /// <summary>
    /// A base class for different types of hurtboxes, ie. regular hurtbox and blockbox
    /// </summary>
    public abstract class AbstractHurtbox : MonoBehaviour
    {
        Collider coll;
        CharacterBase cbase;
        AbstractMover mover;
        [SerializeField, Tooltip("Main character object. Used for position check upon receiving an attack")]
        GameObject parent;

        public CharacterBase Base { get { return cbase; } protected set { cbase = value; } }
        public AbstractMover Mover { get { return mover; } protected set { mover = value; } }
        public GameObject Parent { get { return parent; } protected set { parent = value; } }
        public Collider Box { get { return coll; } protected set { coll = value; } }

        protected virtual void Awake()
        {
            cbase = cbase == null ? GetComponentInParent<CharacterBase>() : cbase;
            Mover = Mover == null ? GetComponentInParent<AbstractMover>() : Mover;
            coll = coll == null ? GetComponent<Collider>() : coll;
            gameObject.tag = cbase.PlayerIdentifier!= "" ? cbase.PlayerIdentifier : "Untagged";
        }

        public abstract void ProcessHit(int hitType, float damage, Vector3 origin, Vector3 knockbackDir, float baseStrength);
    }
}

