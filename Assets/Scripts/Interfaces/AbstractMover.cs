﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace PlatformFighter
{
    public abstract class AbstractMover : MonoBehaviour
    {
        public virtual int JumpsLeft { get; set; }
        public virtual float DirY { get; protected set; }
        public virtual float DirX { get; protected set; }
        public virtual Vector3 MoveDir { get; protected set; }
        public virtual bool Grounded { get; protected set; }
        public virtual bool KnockbackApplied { get; protected set; }
        public virtual Quaternion CharacterRotation { get; protected set; }

        public abstract void Move();


        public abstract void Jump();


        //used for making the character face a received attack
        public abstract void ForceRotation(Quaternion rotation);
        public abstract void ApplyKnockback(Vector3 direction);
        public abstract void AirDodge(Vector3 direction);
    }
}
