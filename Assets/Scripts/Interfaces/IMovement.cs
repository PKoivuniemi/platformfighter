﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformFighter {
    public interface IMovement
    {
        void Move();
        void Jump();
        //void HandleGravity();
    }

}
