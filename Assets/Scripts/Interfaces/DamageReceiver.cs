﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformFighter {
    public abstract class DamageReceiver : MonoBehaviour
    {
        protected abstract Health Health { get; set; }

        public abstract void TakeDamage(float amount);

    }
}

